Antti Sinkkonen
0439700

Viikon 8 tehtävien repo. Viikon 8 tehtävissä tuli tutustua graafisen käyttöliittymän saloihin ja lopulta luoda alkeellinen tekstieditori, jossa voidaan tallentaa ja lukea tekstiä käyttäjän asettamista tiedostoista. 
Tämä repo sisältää viikon viimeisen tehtävän 8.5 lähdekoodin.

**Kaikki koodit ovat Source-branchissa.**